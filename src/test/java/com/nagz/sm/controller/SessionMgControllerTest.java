package com.nagz.sm.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.nagz.sm.entity.Session;
import com.nagz.sm.repo.SessionMgDynamoRepo;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = SessionMgController.class)
public class SessionMgControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	SessionMgDynamoRepo sessionMgDynamoRepo;
	
	private Session session;

	@Before
	public void setup() {
		session = new Session();
		session.setTimestamp("1122334455");
		session.setUsername("mocker");
	 }
	
	@Test
	public void test_get_valid_session_for_200() throws Exception {

		when(sessionMgDynamoRepo.findBySid("112233")).thenReturn(session);
        
		mockMvc.perform(get("/sessions/{sid}", "112233"))
					.andDo(print())
					.andExpect(status().isOk())
					.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
					.andExpect(jsonPath("$.username").value("mocker"));
	}

	@Test
	public void test_get_invalid_session_for_404() throws Exception {

		when(sessionMgDynamoRepo.findBySid("112233")).thenReturn(null);

		mockMvc.perform(get("/sessions/{sid}", "112233"))
					.andDo(print())
					.andExpect(status().isNotFound());
	}

}
