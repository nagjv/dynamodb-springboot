package com.nagz.sm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SessionMgApplication {

	public static void main(String[] args) {
		SpringApplication.run(SessionMgApplication.class, args);
	}
}
