package com.nagz.sm.config;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;

@Configuration
@EnableDynamoDBRepositories("com.nagz.sm.repo")
public class SessionMgConfig {

	private AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder.standard()
			.withRegion(Regions.US_EAST_2)
			.build();  
 
	@Bean
	public AmazonDynamoDB amazonDynamoDB() {
		return amazonDynamoDB;
	}

}