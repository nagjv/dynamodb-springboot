package com.nagz.sm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.nagz.sm.entity.Session;
import com.nagz.sm.exception.SessionNotFoundException;
import com.nagz.sm.repo.SessionMgDynamoRepo;

@RestController
public class SessionMgController {
	
	@Autowired
	SessionMgDynamoRepo smRepo;
	
	@GetMapping(path="/info", produces=MediaType.APPLICATION_JSON_VALUE)
	public String getInfo( ) {
		return "[\"Session Manager 1.0\"]";
	}

	@GetMapping(path="/sessions/{sid}", produces=MediaType.APPLICATION_JSON_VALUE)
	public Session getSessionForSID(@PathVariable String sid) {
		Session session = smRepo.findBySid(sid);
		if(session == null)
			throw new SessionNotFoundException();

		return session;
	}

	@PostMapping(path="/sessions", produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Session createSession(@RequestBody Session session) {
		return smRepo.save(session);
	}
}
