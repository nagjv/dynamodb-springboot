package com.nagz.sm.repo;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import com.nagz.sm.entity.Session;

@EnableScan
public interface SessionMgDynamoRepo extends CrudRepository<Session, String> {

	Session findBySid(String sid);
	
}